import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DevsPage } from './devs.page';

describe('DevsPage', () => {
  let component: DevsPage;
  let fixture: ComponentFixture<DevsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DevsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DevsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
